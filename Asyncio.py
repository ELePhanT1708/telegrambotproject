import asyncio
import time


async def count(counter: list):
    print(f'Quantity in list : {len(counter)}')
    while True:
        await asyncio.sleep(1 / 100)
        counter.append(1)


async def print_one_sec(counter):
    while True:
        await asyncio.sleep(1)
        print(f' Past 1 second, Quantity : {len(counter)}')


async def print_five_sec(counter):
    while True:
        await asyncio.sleep(5)
        print(f' _____Past 5 second, Quantity : {len(counter)}')


async def print_ten_sec(counter):
    while True:
        await asyncio.sleep(10)
        print(f' __________Past 10 second, Quantity : {len(counter)}')


async def main():
    counter = []
    tasks = [count(counter), print_one_sec(counter), print_five_sec(counter)]
    await asyncio.gather(*tasks)


asyncio.run(main())
